/**
 * Created by cedricrouille on 23/02/2016.
 */

export interface Hero {
    id: number;
    name: string;
}

