# TUTORIAL: TOUR OF HEROES #

The Tour of Heroes tutorial takes us through the steps of creating an Angular 2 application in TypeScript.
[Steps here.](https://angular.io/docs/ts/latest/tutorial/)

Based on release 2.0.0-beta.7